FROM openjdk:15
ADD build/libs/moviedatabase-0.0.1.jar emdb.jar
ENTRYPOINT [ "java", "-jar", "emdb.jar" ]