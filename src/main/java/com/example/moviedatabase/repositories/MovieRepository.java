package com.example.moviedatabase.repositories;

import com.example.moviedatabase.models.DTO.MovieDTO;
import com.example.moviedatabase.models.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Long> {
    List<Movie> getAllByFranchise_FranchiseId(long franchiseId);
}
