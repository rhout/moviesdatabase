package com.example.moviedatabase.repositories;

import com.example.moviedatabase.models.Movie;
import com.example.moviedatabase.models.MovieCharacter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CharacterRepository extends JpaRepository<MovieCharacter, Long> {
    List<MovieCharacter> getAllByMovies(Movie movie);
}
