package com.example.moviedatabase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Entry point for the application.
 */
@SpringBootApplication
public class MoviedatabaseApplication {

    public static void main(String[] args) {
        SpringApplication.run(MoviedatabaseApplication.class, args);
    }
}
