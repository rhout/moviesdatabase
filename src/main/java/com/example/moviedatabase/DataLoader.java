//package com.example.moviedatabase;
//
//import com.example.moviedatabase.models.MovieCharacter;
//import com.example.moviedatabase.models.Franchise;
//import com.example.moviedatabase.models.Movie;
//import com.example.moviedatabase.repositories.CharacterRepository;
//import com.example.moviedatabase.repositories.FranchiseRepository;
//import com.example.moviedatabase.repositories.MovieRepository;
//import org.json.simple.JSONArray;
//import org.json.simple.JSONObject;
//import org.json.simple.parser.JSONParser;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.CommandLineRunner;
//import org.springframework.stereotype.Component;
//
//import java.io.FileReader;
//import java.util.*;
//
//@Component
//public class DataLoader implements CommandLineRunner {
//    @Autowired
//    CharacterRepository characterRepository;
//    @Autowired
//    MovieRepository movieRepository;
//    @Autowired
//    FranchiseRepository franchiseRepository;
//
//    @Override
//    public void run(String... args) throws Exception {
//        loadData();
//    }
//
//    private void loadData() {
//
//
//        JSONParser jsonParser = new JSONParser();
//        JSONObject jsonObject = null;
//        try {
//            jsonObject = (JSONObject) jsonParser.parse(new FileReader("src/main/resources/data.json"));
//            JSONArray characters = (JSONArray) jsonObject.get("characters");
//            JSONArray movies = (JSONArray) jsonObject.get("movies");
//            JSONArray franchises = (JSONArray) jsonObject.get("franchises");
//
//            if (franchiseRepository.count() == 0) {
//                for (Object franschise : franchises) {
//                    String name = (String) ((JSONObject) franschise).get("name");
//                    String description = (String) ((JSONObject) franschise).get("description");
//
//                    Franchise f = new Franchise(name, description);
//                    franchiseRepository.save(f);
//                }
//            }
//
//            if (movieRepository.count() == 0) {
//                for (Object movie : movies) {
//                    String title = (String) ((JSONObject) movie).get("title");
//                    String genre = (String) ((JSONObject) movie).get("genre");
//                    long releaseYear = (long) ((JSONObject) movie).get("releaseYear");
//                    String director = (String) ((JSONObject) movie).get("director");
//                    String image = (String) ((JSONObject) movie).get("image");
//                    String trailer = (String) ((JSONObject) movie).get("trailer");
//                    long franchiseId = (long) ((JSONObject) movie).get("franchiseId");
//
//                    Movie m = new Movie(title, genre, releaseYear, director, image, trailer, new Franchise(franchiseId));
//                    movieRepository.save(m);
//                }
//            }
//
//            if (characterRepository.count() == 0) {
//                for (Object character : characters) {
//                    String name = (String) ((JSONObject) character).get("fullName");
//                    String alias = (String) ((JSONObject) character).get("alias");
//                    String gender = (String) ((JSONObject) character).get("gender");
//                    String picture = (String) ((JSONObject) character).get("picture");
//
//                    JSONArray moviesList = (JSONArray) ((JSONObject) character).get("movies");
//                    Set<Movie> moviesIdList = new HashSet<>();
//                    for (Object movieId : moviesList) {
//                        moviesIdList.add(new Movie((long) ((JSONObject) movieId).get("movieId")));
//                    }
//
//                    MovieCharacter c = new MovieCharacter(name, alias, gender, picture, moviesIdList);
//                    characterRepository.save(c);
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//}
