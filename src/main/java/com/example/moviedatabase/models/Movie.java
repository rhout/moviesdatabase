package com.example.moviedatabase.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Where(clause = "active = true")
public class Movie {

    // Variables used for Movies. Auto-increments the ID.
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long movieId;
    private String title;
    private String genre;
    private long releaseYear;
    private String director;
    private String image;
    private String trailer;
    private boolean active = true;

    @ManyToMany(mappedBy = "movies")
    Set<MovieCharacter> characters;

    // Lists all characters in the movie
    @JsonGetter("characters")
    public Set<String> characters() {
        if (characters != null) {
            return characters.stream()
                    .map(character -> "/api/v1/characters/" + character.getCharacterId()).collect(Collectors.toSet());
        } else {
            return new HashSet<>();
        }
    }

    @ManyToOne
    Franchise franchise;

    // Gets the franchise the movie is in
    @JsonGetter("franchise")
    public String author() {
        if (franchise != null) {
            return "/api/v1/franchises/" + franchise.getFranchiseId();
        } else {
            return null;
        }
    }

    // Default constructor
    public Movie() {
    }

    // Constructor
    public Movie(long movieId) {
        this.movieId = movieId;
    }

    // Constructor
    public Movie(String title, String genre, long releaseYear, String director, String image, String trailer, Franchise franchise) {
        this.title = title;
        this.genre = genre;
        this.releaseYear = releaseYear;
        this.director = director;
        this.image = image;
        this.trailer = trailer;
        this.franchise = franchise;
    }

    // Getters and Setters
    public long getMovieId() {
        return movieId;
    }

    public String getTitle() {
        return title;
    }

    public String getGenre() {
        return genre;
    }

    public long getReleaseYear() {
        return releaseYear;
    }

    public String getDirector() {
        return director;
    }

    public String getImage() {
        return image;
    }

    public String getTrailer() {
        return trailer;
    }

    public Franchise getFranchise() {
        return franchise;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Set<MovieCharacter> getCharacters() {
        return characters;
    }
}
