package com.example.moviedatabase.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Where(clause = "active = true")
public class Franchise {

    // Variables used for franchises. Auto-increments the ID
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long franchiseId;
    private String name;
    private String description;
    private boolean active = true;

    @OneToMany(mappedBy = "franchise")
    List<Movie> movies;

    // Lists all movies in the franchise
    @JsonGetter("movies")
    public List<String> movies() {
        if (movies != null) {
            return movies.stream()
                    .map(movie -> "/api/v1/movies/" + movie.getMovieId()).collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    // Default constructor
    public Franchise() {
    }

    // Constructor
    public Franchise(String name, String description) {
        this.name = name;
        this.description = description;
    }

    // Getters and Setters
    public Franchise(long franchiseId) {
        this.franchiseId = franchiseId;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public long getFranchiseId() {
        return franchiseId;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
