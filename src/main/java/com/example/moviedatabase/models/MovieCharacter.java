package com.example.moviedatabase.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "character")
@Where(clause = "active = true")
public class MovieCharacter {

    // Variables used for characters. Auto-increments the ID.
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long characterId;
    private String fullName;
    private String alias;
    private String gender;
    private String picture;
    private boolean active = true;

    @ManyToMany
    @JoinTable(name = "movie_character",
            joinColumns = {@JoinColumn(name = "character_id")},
            inverseJoinColumns = {@JoinColumn(name = "movie_id")}
    )
    Set<Movie> movies;

    // Lists all the movies the character appears in.
    @JsonGetter("movies")
    public Set<String> moviesGetter() {
        if (movies != null) {
            return movies.stream()
                    .map(movie -> "/api/v1/movies/" + movie.getMovieId()).collect(Collectors.toSet());
        }
        return new HashSet<>();
    }

    // Default constructor
    public MovieCharacter() {

    }

    // Constructor
    public MovieCharacter(String fullName, String alias, String gender, String picture, Set<Movie> movies) {
        this.fullName = fullName;
        this.alias = alias;
        this.gender = gender;
        this.picture = picture;
        this.movies = movies;
    }

    // Getters and setters
    public long getCharacterId() {
        return characterId;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Set<Movie> getMovies() {
        return movies;
    }

    public void setMovies(Set<Movie> movies) {
        this.movies = movies;
    }

    public String getFullName() {
        return fullName;
    }

    public String getAlias() {
        return alias;
    }

    public String getGender() {
        return gender;
    }
}
