package com.example.moviedatabase.models.DTO;

public class MovieCharacterDTO {

    // Variables used for CharacterDTOs.
    public long characterId;
    public String fullName;
    public String alias;
    public String picture;

    // Default constructor
    public MovieCharacterDTO() {
    }

    // Constructor
    public MovieCharacterDTO(long characterId, String fullName, String alias, String picture) {
        this.characterId = characterId;
        this.fullName = fullName;
        this.alias = alias;
        this.picture = picture;
    }
}
