package com.example.moviedatabase.models.DTO;

public class MovieDTO {

    // Variables used for MovieDTOs
    public long movieId;
    public String title;
    public long releaseYear;
    public String image;

    // Default constructor
    public MovieDTO() {
    }

    // Constructor
    public MovieDTO(long movieId, String title, long releaseYear, String image) {
        this.movieId = movieId;
        this.title = title;
        this.releaseYear = releaseYear;
        this.image = image;
    }
}
