package com.example.moviedatabase.controllers;

import com.example.moviedatabase.models.Franchise;
import com.example.moviedatabase.services.FranchiseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/franchises")
public class FranchiseController {
    @Autowired
    private FranchiseService franchiseService;

    /**
     * Fetches all the franchises from the database.
     *
     * @return
     */
    @GetMapping()
    public ResponseEntity<List<Franchise>> getAllFranchises() {
        HttpStatus status;
        List<Franchise> franchises = franchiseService.getAllFranchises();
        status = franchises.size() > 0 ? HttpStatus.OK : HttpStatus.BAD_REQUEST;

        return new ResponseEntity<>(franchises, status);
    }

    /**
     * Fetches a specific franchise from the database.
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public ResponseEntity<Franchise> getFranchise(@PathVariable long id) {
        Franchise returnFranchise = franchiseService.getFranchise(id);
        HttpStatus status;

        if (returnFranchise != null) {
            status = HttpStatus.OK;
            return new ResponseEntity<>(returnFranchise, status);
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnFranchise, status);
    }

    /**
     * Adds a new franchise to the database.
     *
     * @param franchise
     * @return
     */
    @PostMapping
    public ResponseEntity<Franchise> addFranchise(@RequestBody Franchise franchise) {
        Franchise returnFranchise = franchiseService.addFranchise(franchise);
        HttpStatus status = HttpStatus.BAD_REQUEST;
        if (returnFranchise != null) {
            status = HttpStatus.CREATED;
            return new ResponseEntity<>(status);
        }
        return new ResponseEntity<>(status);
    }

    /**
     * Updates a specific franchise in the database.
     *
     * @param id
     * @param franchise
     * @return
     */
    @PutMapping("/{id}")
    public ResponseEntity<Franchise> updateFranchise(@PathVariable long id, @RequestBody Franchise franchise) {
        HttpStatus status = HttpStatus.BAD_REQUEST;
        if (id == franchise.getFranchiseId()) {
            Franchise returnFranchise = franchiseService.updateFranchise(franchise);
            if (returnFranchise != null) {
                status = HttpStatus.OK;
                return new ResponseEntity<>(returnFranchise, status);
            }
        }
        return new ResponseEntity<>(null, status);
    }

    /**
     * Soft deletes a specific franchise from the database.
     *
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteFranchise(@PathVariable long id) {
        if (franchiseService.deleteFranchise(id) != null) {
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
