package com.example.moviedatabase.controllers;

import com.example.moviedatabase.models.DTO.MovieCharacterDTO;
import com.example.moviedatabase.models.MovieCharacter;
import com.example.moviedatabase.services.CharacterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/characters")
public class CharacterController {
    @Autowired
    private CharacterService characterService;

    /**
     * Fetches all the active characters from the database.
     *
     * @return
     */
    @GetMapping()
    public ResponseEntity<List<MovieCharacterDTO>> getAllCharacters() {
        List<MovieCharacterDTO> characters = characterService.getAllCharacters();
        if (characters.size() > 0) {
            return ResponseEntity.ok().body(characters);
        }
        return ResponseEntity.notFound().build();
    }

    /**
     * Fetches all the characters that appears in a specific movie.
     *
     * @param movieId
     * @return
     */
    @GetMapping("/movie/{movieId}")
    public ResponseEntity<List<MovieCharacterDTO>> getCharactersInAMovie(@PathVariable long movieId){
        HttpStatus status = HttpStatus.NOT_FOUND;
        List<MovieCharacterDTO> charactersInAMovie = characterService.getAllByMovie(movieId);
        if(charactersInAMovie.size() > 0){
            status = HttpStatus.OK;
            return new ResponseEntity<>(charactersInAMovie, status);
        }

        return new ResponseEntity<>(null, status);
    }

    /**
     * Gets all the characters that appear in a specified franchise.
     *
     * @param franchiseId
     * @return
     */
    @GetMapping("/franchise/{franchiseId}")
    public ResponseEntity<Set<MovieCharacterDTO>> getAllCharactersFromFranchise(@PathVariable long franchiseId) {
        Set<MovieCharacterDTO> characters = characterService.getCharactersFromFranchise(franchiseId);
        if (characters.size() > 0) {
            return ResponseEntity.ok().body(characters);
        }
        return ResponseEntity.notFound().build();
    }

    /**
     * Fetches a character from the database by its id.
     *
     * @param characterId
     * @return
     */
    @GetMapping("/{characterId}")
    public ResponseEntity<MovieCharacter> getCharacter(@PathVariable long characterId) {
        MovieCharacter character = characterService.getCharacter(characterId);
        if (character != null) {
            return ResponseEntity.ok().body(character);
        }
        return ResponseEntity.notFound().build();
    }

    /**
     * Creates a new character in the database.
     *
     * @param character The character to be inserted and all its information.
     * @return
     */
    @PostMapping()
    public ResponseEntity<Void> createCharacter(@RequestBody MovieCharacter character) {
        MovieCharacter characterToUpdate = characterService.createCharacter(character);
        if (characterToUpdate != null) {
            return new ResponseEntity<>(HttpStatus.CREATED);
        }
        return ResponseEntity.badRequest().build();
    }

    /**
     * Updates an existing character.
     *
     * @param character The character to update with all the updated information.
     * @return
     */
    @PutMapping("/{characterId}")
    public ResponseEntity<Void> updateCharacter(@PathVariable long characterId, @RequestBody MovieCharacter character) {
        if (characterId == character.getCharacterId()) {
            MovieCharacter updatedCharacter = characterService.updateCharacter(character);
            if (updatedCharacter != null) {
                return ResponseEntity.ok().build();
            }
        }
        return ResponseEntity.notFound().build();
    }

    /**
     * Soft deletes a character from the database by its id.
     *
     * @param characterId
     * @return
     */
    @DeleteMapping("/{characterId}")
    public ResponseEntity<HttpStatus> deleteCharacter(@PathVariable long characterId) {
        if (characterService.deleteCharacter(characterId) != null) {
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.notFound().build();
    }
}
