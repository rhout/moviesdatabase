package com.example.moviedatabase.controllers;


import com.example.moviedatabase.models.DTO.MovieCharacterDTO;
import com.example.moviedatabase.models.DTO.MovieDTO;
import com.example.moviedatabase.models.Movie;
import com.example.moviedatabase.models.MovieCharacter;
import com.example.moviedatabase.services.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/movies")
public class MovieController {
    @Autowired
    private MovieService movieService;

    /**
     * Fetches all movies from the database.
     *
     */
    @GetMapping()
    public ResponseEntity<List<MovieDTO>> getAllMovies(){
        HttpStatus status;
        List<MovieDTO> movies = movieService.getAllMovies();
        if(movies.size() == 0){
            status = HttpStatus.NOT_FOUND;
        } else {
            status = HttpStatus.OK;
        }

        return new ResponseEntity<>(movies, status);
    }

    /**
     * Fetches all the movies that appears in a specific franchise.
     *
     * @param franchiseId
     * @return
     */
    @GetMapping("/franchise/{franchiseId}")
    public ResponseEntity<List<MovieDTO>> getMovies(@PathVariable long franchiseId) {
        List<MovieDTO> movies = movieService.getAllByFranchiseId(franchiseId);
        if (movies.size() > 0) {
            return ResponseEntity.ok().body(movies);
        }
        return ResponseEntity.notFound().build();
    }

    /**
     * Fetches a movie from the database from its id.
     *
     * @param movieId
     * @return
     */
    @GetMapping("/{movieId}")
    public ResponseEntity<Movie> getMovieById(@PathVariable long movieId){
        HttpStatus status = HttpStatus.NOT_FOUND;
        Movie returnMovie = movieService.getMovieById(movieId);
        if(returnMovie != null){
            status = HttpStatus.OK;
            return new ResponseEntity<>(returnMovie, status);
        }

        return new ResponseEntity<>(null, status);
    }

    /**
     * Adds a new movie to the database.
     *
     * @param movie
     * @return
     */
    @PostMapping()
    public ResponseEntity<Movie> createMovie(@RequestBody Movie movie){
        HttpStatus status = HttpStatus.BAD_REQUEST;
        Movie returnMovie = movieService.createMovie(movie);
        if (returnMovie != null) {
            status = HttpStatus.OK;
            return new ResponseEntity<>(returnMovie, status);
        }
        return new ResponseEntity<>(returnMovie, status);
    }

    /**
     * Updates an existing movie in the database.
     *
     * @param movie
     * @return
     */
    @PutMapping("/{movieId}")
    public ResponseEntity<Movie> updateMovie(@PathVariable long movieId, @RequestBody Movie movie){
        HttpStatus status = HttpStatus.NOT_FOUND;
        if (movieId == movie.getMovieId()) {
            Movie returnMovie = movieService.updateMovie(movie);
            if (returnMovie != null) {
                status = HttpStatus.OK;
                return new ResponseEntity<>(returnMovie, status);
            }
        }
        return new ResponseEntity<>(null, status);
    }

    /**
     * Deletes as a movie from the database.
     *
     * @param movieId
     * @return
     */
    @DeleteMapping("/{movieId}")
    public ResponseEntity<HttpStatus> deleteMovie(@PathVariable long movieId){
        HttpStatus status = movieService.deleteMovie(movieId) != null ? HttpStatus.OK : HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(status);
    }
}
