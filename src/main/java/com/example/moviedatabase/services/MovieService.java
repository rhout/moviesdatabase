package com.example.moviedatabase.services;

import com.example.moviedatabase.models.DTO.MovieCharacterDTO;
import com.example.moviedatabase.models.DTO.MovieDTO;
import com.example.moviedatabase.models.Movie;
import com.example.moviedatabase.models.MovieCharacter;
import com.example.moviedatabase.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MovieService {

    @Autowired
    private MovieRepository movieRepository;

    /**
     * Fetches all movies from the database.
     */
    public List<MovieDTO> getAllMovies() {
        List<Movie> m = movieRepository.findAll();
        return convertModelToDTO(m);
    }

    /**
     * Gets all the movies that appears in a specific franchise from the database.
     *
     * @param franchiseId
     * @return
     */
    public List<MovieDTO> getAllByFranchiseId(long franchiseId) {
        return convertModelToDTO(movieRepository.getAllByFranchise_FranchiseId(franchiseId));
    }

    /**
     * Fetches a movie from the database from its id.
     *
     * @param movieId
     * @return
     */
    public Movie getMovieById(long movieId) {
        Movie returnMovie = null;
        boolean exists = movieRepository.existsById(movieId);
        if (exists) {
            returnMovie = movieRepository.findById(movieId).get();
        }

        return returnMovie;
    }

    /**
     * Adds a new movie to the database.
     *
     * @param movie
     * @return
     */
    public Movie createMovie(Movie movie) {
        return movieRepository.save(movie);
    }

    /**
     * Updates an existing movie in the database.
     *
     * @param movie
     * @return
     */
    public Movie updateMovie(Movie movie) {
        Movie returnMovie = null;
        if (movieRepository.existsById(movie.getMovieId())) {
            returnMovie = movieRepository.save(movie);
        }
        return returnMovie;
    }

    /**
     * Deletes as a movie from the database
     *
     * @param movieId
     * @return
     */
    public Movie deleteMovie(long movieId) {
        Movie returnMovie = null;

        if (movieRepository.existsById(movieId)) {
            returnMovie = movieRepository.findById(movieId).get();
            returnMovie.setActive(false);
            movieRepository.save(returnMovie);
        }
        return returnMovie;
    }

    /**
     * Takes a list of Movie and converts it to a list of MovieDTOs.
     *
     * @param modelList
     * @return
     */
    private List<MovieDTO> convertModelToDTO(List<Movie> modelList) {
        List<MovieDTO> dtos = new ArrayList<>();
        for (Movie movie : modelList) {
            dtos.add(new MovieDTO(movie.getMovieId(), movie.getTitle(), movie.getReleaseYear(), movie.getImage()));
        }
        return dtos;
    }
}
