package com.example.moviedatabase.services;

import com.example.moviedatabase.models.DTO.MovieCharacterDTO;
import com.example.moviedatabase.models.DTO.MovieDTO;
import com.example.moviedatabase.models.Movie;
import com.example.moviedatabase.models.MovieCharacter;
import com.example.moviedatabase.repositories.CharacterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class CharacterService {

    @Autowired
    private CharacterRepository characterRepository;

    @Autowired
    private MovieService movieService;

    /**
     * Fetches all the active characters from the database.
     *
     * @return
     */
    public List<MovieCharacterDTO> getAllCharacters() {
        return convertModelToDTO(characterRepository.findAll());
    }

    /**
     * Gets all characters from the database.
     *
     * @param movieId
     * @return
     */
    public List<MovieCharacterDTO> getAllByMovie(long movieId) {
        Movie movie = movieService.getMovieById(movieId);
        List<MovieCharacter> returnCharacterInMovie = characterRepository.getAllByMovies(movie);
        return convertModelToDTO(returnCharacterInMovie);
    }

    /**
     * Gets all the characters that appear in a specified franchise.
     *
     * @param franchiseId
     * @return
     */
    public Set<MovieCharacterDTO> getCharactersFromFranchise(long franchiseId) {
        List<MovieDTO> moviesFromFranchise = movieService.getAllByFranchiseId(franchiseId);
        Set<MovieCharacterDTO> characters = new HashSet<>();
        for (MovieDTO movie : moviesFromFranchise) {
            characters.addAll(getAllByMovie(movie.movieId));
        }
        return characters;
    }

    /**
     * Fetches a character from the database by its id.
     *
     * @param characterId
     * @return
     */
    public MovieCharacter getCharacter(long characterId) {
        MovieCharacter character = null;
        if (characterRepository.existsById(characterId)) {
            character = characterRepository.findById(characterId).get();
        }
        return character;
    }

    /**
     * Creates a new character in the database.
     *
     * @param character The character to be inserted and all its information.
     * @return
     */
    public MovieCharacter createCharacter(MovieCharacter character) {
        return characterRepository.save(character);
    }

    /**
     * Updates an existing character.
     *
     * @param character The character to update with all the updated information.
     * @return
     */
    public MovieCharacter updateCharacter(MovieCharacter character) {
        MovieCharacter characterToUpdate = null;
        if (characterRepository.existsById(character.getCharacterId())) {
            characterToUpdate = characterRepository.save(character);
        }
        return characterToUpdate;
    }

    /**
     * Soft deletes a character from the database by its id.
     *
     * @param characterId
     * @return The deleted character
     */
    public MovieCharacter deleteCharacter(long characterId) {
        MovieCharacter character = null;
        if (characterRepository.existsById(characterId)) {
            character = characterRepository.findById(characterId).get();
            character.setActive(false);
            characterRepository.save(character);
        }
        return character;
    }

    /**
     * Takes a list of MovieCharacters and converts it to a list of MovieCharacterDTOs.
     *
     * @param modelList
     * @return
     */
    private List<MovieCharacterDTO> convertModelToDTO(List<MovieCharacter> modelList) {
        List<MovieCharacterDTO> dtos = new ArrayList<>();
        for (MovieCharacter character : modelList) {
            dtos.add(new MovieCharacterDTO(character.getCharacterId(), character.getFullName(), character.getAlias(), character.getPicture()));
        }
        return dtos;
    }
}
