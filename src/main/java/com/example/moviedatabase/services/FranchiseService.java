package com.example.moviedatabase.services;

import com.example.moviedatabase.models.Franchise;
import com.example.moviedatabase.repositories.FranchiseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Service
public class FranchiseService {

    @Autowired
    private FranchiseRepository franchiseRepository;


    /**
     * Gets all the franchies from the database.
     *
     * @return
     */
    public List<Franchise> getAllFranchises() {
        return franchiseRepository.findAll();
    }

    // getting one franchise by its id
    public Franchise getFranchise(long id) {
        Franchise returnFranchise = null;
        // We first check if the Library exists, this saves some computing time.
        if (franchiseRepository.existsById(id)) {
            returnFranchise = franchiseRepository.findById(id).get();
        }
        return returnFranchise;
    }


    /**
     * Add a new franchise to the database.
     *
     * @param franchise
     * @return
     */
    public Franchise addFranchise(@RequestBody Franchise franchise) {
        return franchiseRepository.save(franchise);
    }


    /**
     * Update an existing franchise to the database.
     *
     * @param franchise
     * @return
     */
    public Franchise updateFranchise(Franchise franchise) {
        Franchise returnFranchise = null;
        if (franchiseRepository.existsById(franchise.getFranchiseId())) {
            returnFranchise = franchiseRepository.save(franchise);
        }
        return returnFranchise;
    }

    /**
     * Soft delete a specific franchise from the database.
     *
     * @param franchiseId
     * @return
     */
    public Franchise deleteFranchise(long franchiseId) {
        Franchise returnFranchise = null;
        if (franchiseRepository.existsById(franchiseId)) {
            returnFranchise = franchiseRepository.findById(franchiseId).get();
            returnFranchise.setActive(false);
            franchiseRepository.save(returnFranchise);
        }
        return returnFranchise;
    }
}
