# :movie_camera: Experis Movie Database (EMDB) :movie_camera:
### By [Tanja Vinogradova](https://gitlab.com/tavinogradova92), [Andreas Kjelstrup](https://gitlab.com/AndreasKjelstrup) and [Rune Hou Thode](https://gitlab.com/rhout)
A movie database with various exciting posibilities which are:
- CRUD on franchises, movies and movie characters
- See all the movies made in a franchise
- Get a list of all characters starring in a movie
- You can even get all the characters that star in a given franchise!

## Requirements
This application is created using JDK15 and has not been tested on any other versions.

The data is stored in a postgres database, so to create a local database you can work with, download and install [PostgreSQL](https://www.postgresql.org/). Create a database in pgAdmin called MovieDatabase (use default port 5432) or update the *spring.datasource.url* in [application.properties](./src/main/resources/aaplication.properties).
**Note:** You should never store your password in plaintext.


<br>

## Home page
The app is deployed using Heroku and the API endpoints start at https://experis-mdb.herokuapp.com/api/v1. The documentation for the endpoints can be found [here](https://experis-mdb.herokuapp.com/api/v1/swagger-ui/index.html).

## Postman
To test all the endpoints, a postman collection has been places in the [test folder](./src/test/postman_tests.json).